
angular.module('myApp', ['ui.grid'])
.controller('MainCtrl', ['$scope', function($scope) {
    $scope.gridOptions = {
        columnDefs: [
            { name: 'id', displayName: 'ID' },
            { name: 'name', displayName: 'Name',cellTemplate:'customCellTemplate.html'},
            { name: 'status', displayName: 'Status',cellTemplate:'customCellTemplate.html' }
        ],
        data: [
            { id: 1, name: 'Item 1', status: 'active' },
            { id: 2, name: 'Item 2', status: 'inactive' },
            { id: 3, name: 'Item 3', status: 'active' }
        ],
        rowTemplate:'customRowTemplate.html'
    };
    $scope.deleteRow=function(entity){
        var index=$scope.gridOptions.data.indexOf(entity);
        if(index !== -1){
            $scope.gridOptions.data.splice(index,1);
            console.log('Deleted',entity);
        } 
    };
}]);
