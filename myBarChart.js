Highcharts.chart('container',{
    chart:{
        type:'bar'
    },
    title:{
        text:'Run Scored By cricketers Per Year',
        align:'left'
    },
    subtitle:{
        text:'www.crickinfo.com',
        align:'left'
    },
    xAxis:{
        categories:['Virat Kohli','Rohit Sharma','Shubhman gill','Hardik pandya'],
        title:{
            text:'Cricketers'
        },
        gridLineWidth:1,
        lineWidth:0
    },

    yAxis: {
        min: 0,
        title: {
            text: 'Runs',
            align: 'high'
        },
        labels: {
            overflow: 'justify'
        },
        gridLineWidth: 0
    },
    tooltip: {
        valueSuffix: ' runs'
    },
    plotOptions: {
        bar: {
            borderRadius: '50%',
            dataLabels: {
                enabled:true
            },
            groupPadding: 0.1
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'top',
        x: -10,
        y: 70,
        floating: true,
        borderWidth: 1,
        backgroundColor:
            Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
        shadow: true
    },
    credits: {
        enabled: false
    },
    series:[
        {
            name:'Year 2019',
            data:[2000,1900,1800,1700]
        },
        {
            name:'Year 2020',
            data:[3000,2700,2200,2680]
        },
        {
            name:'Year 2021',
            data:[3200,2700,2780,2700]
        },
        {
            name:'Year 2022',
            data:[3400,3900,2800,3700]
        },
    ]
})