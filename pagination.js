
var app = angular.module('myApp', ['ui.grid','ui.grid.pagination']);
app.controller('ListController',['$scope',function($scope){
    $scope.data=[
        {
           sno: 1,
           name: 'Sachin Tendulkar',
           role: 'Batsman',
           nationality: 'Indian',
           centuries: 100,
           active: "No",
       },
       {
           sno: 2,
           name: 'Ricky Ponting',
           role: 'Batsman',
           nationality: 'Australian',
           centuries: 71,
           active: "No",
       },
       {
           sno: 3,
           name: 'Virat Kohli',
           role: 'Batsman',
           nationality: 'Indian',
           centuries: 68,
           active: "Yes",
       },
       {
           sno: 4,
           name: 'Kumar Sangakkara',
           role: 'Wicketkeeper Batsman',
           nationality: 'Sri Lankan',
           centuries: 63,
           active: "No",
       },
       {
           sno: 5,
           name: 'Jacques Kallis',
           role: 'All Rounder',
           nationality: 'South African',
           centuries: 62,
           active: "No",
       },
       {
           sno: 6,
           name: 'Hashim Amla',
           role: 'Batsman',
           nationality: 'South Indian',
           centuries: 55,
           active: "No",
       },
       {
           sno: 7,
           name: 'Mahela Jayawardene',
           role: 'Batsman',
           nationality: 'Sri Lankan',
           centuries: 54,
           active: "No",
       },
       {
           sno: 8,
           name: 'Brian Lara',
           role: 'Batsman',
           nationality: 'West Indies',
           centuries: 53,
           active: "No",
       },
       {
           sno: 9,
           name: 'Rahul Dravid',
           role: 'Batsman',
           nationality: 'Indian',
           centuries: 48,
           active: "No",
       },
       {
           sno: 10,
           name: 'AB de Villiers',
           role: 'Wicketkeeper Batsman',
           nationality: 'South African',
           centuries: 47,
           active: "No",
       },
       {
        sno: 11,
        name: 'Sachin Tendulkar',
        role: 'Batsman',
        nationality: 'Indian',
        centuries: 100,
        active: "No",
    },
    {
        sno: 12,
        name: 'Ricky Ponting',
        role: 'Batsman',
        nationality: 'Australian',
        centuries: 71,
        active: "No",
    },
    {
        sno: 13,
        name: 'Virat Kohli',
        role: 'Batsman',
        nationality: 'Indian',
        centuries: 68,
        active: "Yes",
    },
    {
        sno: 14,
        name: 'Kumar Sangakkara',
        role: 'Wicketkeeper Batsman',
        nationality: 'Sri Lankan',
        centuries: 63,
        active: "No",
    },
    {
        sno: 15,
        name: 'Jacques Kallis',
        role: 'All Rounder',
        nationality: 'South African',
        centuries: 62,
        active: "No",
    },
    {
        sno: 16,
        name: 'Hashim Amla',
        role: 'Batsman',
        nationality: 'South Indian',
        centuries: 55,
        active: "No",
    },
    {
        sno: 17,
        name: 'Mahela Jayawardene',
        role: 'Batsman',
        nationality: 'Sri Lankan',
        centuries: 54,
        active: "No",
    },
    {
        sno: 18,
        name: 'Brian Lara',
        role: 'Batsman',
        nationality: 'West Indies',
        centuries: 53,
        active: "No",
    },
    {
        sno: 19,
        name: 'Rahul Dravid',
        role: 'Batsman',
        nationality: 'Indian',
        centuries: 48,
        active: "No",
    },
    {
        sno: 20,
        name: 'AB de Villiers',
        role: 'Wicketkeeper Batsman',
        nationality: 'South African',
        centuries: 47,
        active: "No",
    }
   
   ];
   $scope.gridOptions={
    data:'data',
    paginationPageSizes:[5,10,15],
    paginationPageSize:5,
    columnDefs:[
        { name: 'sno', displayName: 'Sno'},
        { name: 'name', displayName: 'Name' },
        { name: 'role', displayName: 'Role' },
        { name: 'nationality', displayName: 'Nationality' },
        { name: 'centuries', displayName: 'Centuries' },
        { name: 'active', displayName: 'Active' }
    ]
   }

 
}]);
 