var app = angular.module('app', ['ui.grid']);

app.controller('MainCtrl', ['$scope', function ($scope) {

  $scope.data = [
    { id: 1, name: 'Alice', age: 28, gender: 'female' },
    { id: 2, name: 'Bob', age: 35, gender: 'male' },
    { id: 3, name: 'Charlie', age: 22, gender: 'male' },
   
  ];

  $scope.gridOptions = {
    enableRowSelection: true,
    enableSelectAll: false,
    enableRowHeaderSelection: false,
    selectionRowHeaderWidth: 35,
    multiSelect: false,
    data: $scope.data,
    columnDefs: [
      { name: 'name', displayName: 'Name' },
      { name: 'age', displayName: 'Age' },
      { name: 'gender', displayName: 'Gender' }
    ],
    onRegisterApi: function(gridApi) {
        
        
      $scope.gridApi = gridApi;

      
     $scope.gridApi.selection.on.rowSelectionChanged($scope, $scope.rowSelectionChanged);
     
      // gridApi.selection.on.rowSelectionChangedBatch($scope, function(rows) {
      //   console.log('Batch selection changed:', rows);
      // });
    }

  };
  $scope.rowSelectionChanged=function(row){
    console.log('Row selected:', row.entity);
  };
 
}]);
