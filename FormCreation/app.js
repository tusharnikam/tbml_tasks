angular.module('myApp', [])
.controller('FormController', function ($scope) {
    $scope.formData = {
        name: '',
        email: '',
        password: ''
    };

    $scope.submitForm = function () {
        if ($scope.registrationForm.$valid) {
           
            console.log('Form data submitted:', $scope.formData);
            
        } else {
            console.log('Form is invalid. Please check errors.');
        }
    };
});

