var app = angular.module('app', [ 'ui.grid']);

app.controller('MainCtrl', ['$scope', function ($scope) {
  
  $scope.data = [
    { id: 1, name: 'Alice', age: 28, gender: 'female' },
    { id: 2, name: 'Bob', age: 35, gender: 'male' },
    { id: 3, name: 'Charlie', age: 22, gender: 'male' },
    
  ];


  $scope.gridOptions = {
    enableFiltering: true,
    enableSorting: true,
    enableRowSelection: true,
    enableSelectAll: true,
    selectionRowHeaderWidth: 35,
    multiSelect: true,
    modifierKeysToMultiSelect: true,
    noUnselect: false,
    
    columnDefs: [
      { name: 'name', displayName: 'Name' },
      { name: 'age', displayName: 'Age' },
      { name: 'gender', displayName: 'Gender' }
    ],
    data: 'data'
  };
}]);

